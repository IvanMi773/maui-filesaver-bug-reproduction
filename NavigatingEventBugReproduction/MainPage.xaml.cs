﻿using System.Diagnostics;

namespace NavigatingEventBugReproduction;

public partial class MainPage : ContentPage
{
	public MainPage()
	{
		InitializeComponent();
	}

	private void wvWebApp_Navigating(System.Object sender, Microsoft.Maui.Controls.WebNavigatingEventArgs e)
	{
		Debug.WriteLine(e.Url.ToString());
	}
}
