﻿using Android;
using Android.App;
using Android.Content.PM;
using Android.OS;

namespace NavigatingEventBugReproduction;

[Activity(Theme = "@style/Maui.SplashTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize | ConfigChanges.Density)]
public class MainActivity : MauiAppCompatActivity
{
	protected override void OnCreate(Bundle savedInstanceState)
	{
		base.OnCreate(savedInstanceState);

		RequestStorageReadAndWritePermissions();
	}

	private void RequestStorageReadAndWritePermissions()
	{
		var isReadStorageAllowed = CheckSelfPermission(Manifest.Permission.ReadExternalStorage) == Permission.Granted;
		var isWriteStorageAllowed = CheckSelfPermission(Manifest.Permission.WriteExternalStorage) == Permission.Granted;
		var permissionsToRequest = new List<string>();

		if (!isReadStorageAllowed)
		{
			permissionsToRequest.Add(Manifest.Permission.ReadExternalStorage);
		}

		if (!isWriteStorageAllowed)
		{
			permissionsToRequest.Add(Manifest.Permission.WriteExternalStorage);
		}

		if (permissionsToRequest.Any())
		{
			const int requestStorePermissionsId = 123;
			RequestPermissions(permissionsToRequest.ToArray(), requestStorePermissionsId);
		}
	}
}
